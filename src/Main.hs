{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Control.Concurrent            (forkIO)
import           Control.Monad                 (void)
import           Control.Monad.IO.Class        (liftIO)
import           Data.Aeson                    (Value, object, (.=))
import           Data.Semigroup                ((<>))
import qualified Data.Text                     as T
import qualified Data.Text.Lazy                as TL
import           Lambdabot.Run                 (lambdabot)
import           Network.Wai.Middleware.Static (addBase, staticPolicy)
import           Network.Wreq                  (FormParam ((:=)))
import qualified Network.Wreq                  as Wreq
import           Paths_lambdabot_slack         (getDataFileName)
import           System.Environment            (getEnv)
import           Web.Scotty

code :: [String] -> String
code ls =
  unlines (["```"] ++ ls ++ ["```"])

inChannel :: [(T.Text, Value)]
inChannel =
  [ "response_type" .= ("in_channel" :: String) ]

publicResponse :: String -> Value
publicResponse result =
  object $ inChannel <> [ "text" .= result ]

getClientId :: IO String
getClientId =
  liftIO $ getEnv "SLACK_CLIENT_ID"

main :: IO ()
main = do
  static <- getDataFileName "static"
  scotty 8080 $ do
    middleware . staticPolicy $ addBase static
    let
      run command = do
        r <- param "response_url"
        void . liftIO . forkIO $ do
          result <- lambdabot command
          void . Wreq.post r . publicResponse $ code result
        json $ object inChannel
    post "/slack/slash/lambdabot" $ do
      t <- param "text"
      appId <- liftIO $ getEnv "SLACK_APP_ID"
      if t == "help"
      then text $ "Visit https://slack.com/apps/" <> TL.pack appId <> " for usage\nContact brian@brianmckenna.org for support"
      else run t
    post "/slack/slash" $ do
      c <- dropWhile (/= '/') <$> param "command"
      t <- param "text"
      run $ unwords [c, t]
    get "/slack/oauth" $ do
      clientId <- liftIO getClientId
      appId <- liftIO $ getEnv "SLACK_APP_ID"
      clientSecret <- liftIO $ getEnv "SLACK_CLIENT_SECRET"
      c <- param "code"
      r <- liftIO $ Wreq.post "https://slack.com/api/oauth.access"
        [ "client_id" := clientId
        , "client_secret" := clientSecret
        , "code" := (c :: String)
        ]
      redirect . TL.pack $ "https://slack.com/app_redirect?app=" ++ appId
    get "/slack/direct" $ do
      clientId <- liftIO getClientId
      redirect . TL.pack $ "https://slack.com/oauth/authorize?client_id=" ++ clientId ++ "&scope=commands"
