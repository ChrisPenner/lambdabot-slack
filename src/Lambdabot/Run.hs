-- | Largely based on the @offlineRC@ plugin.

module Lambdabot.Run (
  lambdabot
) where

import           Codec.Binary.UTF8.String  (decodeString, encodeString)
import           Control.Concurrent.Lifted (fork)
import           Control.Exception.Lifted  (finally)
import           Control.Monad             (void)
import           Control.Monad.Trans       (liftIO)
import           Data.IORef
import           Data.Some                 (Some (..))
import           Lambdabot.IRC
import           Lambdabot.Main
import           Lambdabot.Monad
import           Lambdabot.Plugin
import           Lambdabot.Plugin.Haskell
import           System.Timeout.Lifted     (timeout)

type Messages = IORef [String]
type IORefPlugin = ModuleT Messages LB

ioRefPlugin :: String -> Messages -> Module Messages
ioRefPlugin c s =
  newModule { moduleDefState = pure s
            , moduleInit =
                void . fork $ do
                  waitForInit
                  registerServer "offlinerc" handleMsg
                  feed c `finally` unregisterServer "offlinerc"
            }

feed :: String -> IORefPlugin ()
feed msg = do
    cmdPrefix <- fmap head (getConfig commandPrefixes)
    let msg' = case msg of
            '>':xs -> cmdPrefix ++ "run " ++ xs
            '!':xs -> xs
            _      -> cmdPrefix ++ dropWhile (== ' ') msg
    -- note that `msg'` is unicode, but lambdabot wants utf-8 lists of bytes
    lb . void . timeout (15 * 1000 * 1000) . received $
              IrcMessage { ircMsgServer = "offlinerc"
                         , ircMsgLBName = "offline"
                         , ircMsgPrefix = "null!n=user@null"
                         , ircMsgCommand = "PRIVMSG"
                         , ircMsgParams = ["offline", ":" ++ encodeString msg' ] }

handleMsg :: IrcMessage -> IORefPlugin ()
handleMsg msg = do
  r <- readMS
  let str = case (tail . ircMsgParams) msg of
          []    -> []
          (x:_) -> tail x
  -- str contains utf-8 list of bytes; convert to unicode
  liftIO $ modifyIORef r (++ [ decodeString str ])

-- | Provides a way of calling into Lambdabot, without forking.
lambdabot :: String -> IO [String]
lambdabot c = do
  s <- newIORef []
  void $ lambdabotMain [ ("base", This basePlugin)
                       , ("version", This versionPlugin)

                       , ("check", This checkPlugin)
                       , ("djinn", This djinnPlugin)
                       , ("eval", This evalPlugin)
                       , ("free", This freePlugin)
                       , ("haddock", This haddockPlugin)
                       , ("hoogle", This hooglePlugin)
                       , ("instances", This instancesPlugin)
                       , ("pl", This plPlugin)
                       , ("pointful", This pointfulPlugin)
                       , ("pretty", This prettyPlugin)
                       , ("source", This sourcePlugin)
                       , ("type", This typePlugin)
                       , ("undo", This undoPlugin)
                       , ("unmtl", This unmtlPlugin)

                       , ("ioref", This $ ioRefPlugin c s)
                       ] [ ]
  readIORef s
