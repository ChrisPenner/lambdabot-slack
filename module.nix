{ pkgs, ... }:

let
  lambdabot-slack =
    pkgs.callPackage ./wrapper.nix { };
in
{
  config.systemd.services.lambdabot-slack = {
    description = "lambdabot-slack Service";
    after = [ "local-fs.target" "network.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${lambdabot-slack}/bin/lambdabot-slack";
      ExecReload = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
      EnvironmentFile = "/var/lib/lambdabot/lambdabot-slack.env";
      User = "lambdabot";
      Group = "lambdabot";
    };
  };
}
