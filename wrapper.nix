{ stdenv, haskellPackages, mueval, writeShellScriptBin }:

let
  lambdabot-slack = haskellPackages.callPackage ./lambdabot-slack.nix { };
  lambdabotPackageSet = hpkgs: [
    hpkgs.lambdabot-trusted
    hpkgs.lens

    hpkgs.boxes
    hpkgs.brick
    hpkgs.esqueleto
    hpkgs.fsnotify
    hpkgs.Glob
    hpkgs.gloss
    hpkgs.gtk
    hpkgs.hamlet
    hpkgs.haskeline
    hpkgs.haskell-src-exts
    hpkgs.hastache
    hpkgs.haxl
    hpkgs.ini
    hpkgs.jwt
    hpkgs.JuicyPixels
    hpkgs.postgresql-simple
    hpkgs.recursion-schemes
    hpkgs.shake
    hpkgs.servant-client
    hpkgs.servant-server
    hpkgs.these
    hpkgs.trifecta
    hpkgs.turtle
  ] ++ mueval.defaultPkgs hpkgs;
  binPaths = [
    (haskellPackages.ghcWithHoogle lambdabotPackageSet)
    (mueval.override { inherit haskellPackages; packages = lambdabotPackageSet; })
    haskellPackages.djinn
  ];
in
writeShellScriptBin "lambdabot-slack" ''
  PATH="${stdenv.lib.makeBinPath binPaths}:$PATH"
  exec ${lambdabot-slack}/bin/lambdabot-slack
''
