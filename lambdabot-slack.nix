{ mkDerivation, aeson, base, dependent-sum, lambdabot-core
, lambdabot-haskell-plugins, lifted-base, mtl, scotty, stdenv, text
, utf8-string, wai-middleware-static, wreq
}:
mkDerivation {
  pname = "lambdabot-slack";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    aeson base dependent-sum lambdabot-core lambdabot-haskell-plugins
    lifted-base mtl scotty text utf8-string wai-middleware-static wreq
  ];
  license = stdenv.lib.licenses.bsd3;
}
